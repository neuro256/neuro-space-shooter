﻿using UnityEngine;

public class BGScroller : MonoBehaviour {

    [Tooltip("Moving speed on Y axis in local space")]
    public float speed;

    //moving the object with the defined speed
    private void Update()
    {
        transform.Translate(Vector3.up * speed * Time.deltaTime);
    }
}

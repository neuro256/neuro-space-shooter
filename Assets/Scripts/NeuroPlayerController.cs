﻿using System;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, yMin, yMax;
}

public class NeuroPlayerController : MonoBehaviour {

    public float speed;
    public Boundary boundary;

    public GameObject shot; // Объект выстрела 
    public Transform shotSpawn; 
    public float fireRate; // Задержка между выстрелами 
    public SimpleTouchPad touchPad;
    public SimpleTouchAreaButton areaButton;

    private Rigidbody2D m_rigidbody;
    private float nextFire;
    private Quaternion calibrationQuaternion;

    private void Start()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
        CalibrateAccelerometer();
    }

    private void CalibrateAccelerometer()
    {
        Vector3 accelerationSpapshot = Input.acceleration;
        Quaternion rotateQuaternion = Quaternion.FromToRotation(new Vector3(0.0f, 0.0f, -1.0f), accelerationSpapshot);
        calibrationQuaternion = Quaternion.Inverse(rotateQuaternion);
    }

    Vector3 FixAcceleration (Vector3 acceleration)
    {
        Vector3 fixedAcceleration = calibrationQuaternion * acceleration;
        return fixedAcceleration;
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetButton("Fire1") && Time.time > nextFire)
#elif UNITY_ANDROID
        if (areaButton.CanFire() && Time.time > nextFire)
#endif
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            GetComponent<AudioSource>().Play();
        }
    }

    private void FixedUpdate()
    {
#if UNITY_EDITOR
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector2 movement = new Vector2(moveHorizontal, moveVertical);
#elif UNITY_ANDROID
        // acceleration movement
        //Vector3 accelerationRaw = Input.acceleration;
        //Vector3 acceleration = FixAcceleration(accelerationRaw);
        //Vector3 movement = new Vector3(acceleration.x, 0.0f, acceleration.y);

        // touchzone movement
        Vector2 direction = touchPad.GetDirection();
        Vector2 movement = new Vector3(direction.x, direction.y);
#endif

        m_rigidbody.velocity = movement * speed;

        m_rigidbody.position = new Vector2(
            Mathf.Clamp(m_rigidbody.position.x, boundary.xMin, boundary.xMax),
            Mathf.Clamp(m_rigidbody.position.y, boundary.yMin, boundary.yMax)
            );
    }
}

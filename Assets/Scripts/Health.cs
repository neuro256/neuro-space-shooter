﻿using System.Collections;
using UnityEngine;

public class Health : MonoBehaviour {

    public int healthPointsCount = 5;
    public float repeatDamagePeriod = 0.2f;
    public int damageAmount = 1;
    public GameObject explosion;
    public HealthViewer healthViewer;

    private float lastHitTime;
    private GameController gameController;
    private int currentHealthPointCount;
    private Animator anim;

    private void Start()
    {
        healthViewer.CreateHealthCounter(healthPointsCount);
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find GameController script");
        }
        anim = GetComponent<Animator>();
        currentHealthPointCount = healthPointsCount;
        lastHitTime = Time.time;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            if(Time.time > lastHitTime + repeatDamagePeriod)
            {    
                if (currentHealthPointCount > 1)
                {
                    lastHitTime = Time.time;
                }
                else
                {
                    Instantiate(explosion, transform.position, transform.rotation);
                    gameController.GameOver();
                    Destroy(gameObject);
                }
                TakeDamage();
            }
        }
    }

    private void TakeDamage()
    {
        anim.SetBool("Hit", true);
        StartCoroutine("StopHit");
        currentHealthPointCount -= damageAmount;
        healthViewer.UpdateHealthCounter(currentHealthPointCount);
    }

    private IEnumerator StopHit()
    {
        yield return new WaitForSeconds(repeatDamagePeriod / 10f);
        anim.SetBool("Hit", false);
    }
}

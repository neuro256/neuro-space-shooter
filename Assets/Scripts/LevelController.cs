﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class EnemyWaves
{
    [Tooltip("time for wave generation from the moment the game started")]
    public float timeToStart;

    [Tooltip("Enemy wave's prefab")]
    public GameObject wave;
}

public class LevelController : MonoBehaviour {

    public EnemyWaves[] enemyWaves;
    //public float timeForNewPowerUp;
    //public GameObject[] planets;
    //public float timeBeetweenPlanets;
    //List<GameObject> planetsList = new List<GameObject>();

    private void Start()
    {
        for (int i = 0; i < enemyWaves.Length; i++)
        {
            StartCoroutine(CreateEnemyWave(enemyWaves[i].timeToStart, enemyWaves[i].wave));
        }
    }

    //Create a new wave after a delay
    IEnumerator CreateEnemyWave(float delay, GameObject Wave)
    {
        if (delay != 0)
            yield return new WaitForSeconds(delay);
        Instantiate(Wave);
    }
}

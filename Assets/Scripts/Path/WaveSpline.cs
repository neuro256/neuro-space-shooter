﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class Shooting
{
    [Range(0, 100)]
    [Tooltip("probability with which the ship of this wave will make a shot")]
    public int shotChance;

    [Tooltip("min and max time from the beginning of the path when the enemy can make a shot")]
    public float shotTimeMin, shotTimeMax;
}

public class WaveSpline : MonoBehaviour {

    public GameObject enemy; 
    public int count; // a number of enemies in the wave
    public float speed; // path passage speed;
    public float timeBetween; // time between emerging of the enemies in the wave
    public Transform[] pathPoints; // points of the path
    public bool rotationByPath; // wheither Enemy rotates in the path passage direction
    public bool Loop; // if loop is activated, after completing the path 'Enemy' will return to the starting point
    public Color pathColor = Color.yellow; // color of the path in the Editor
    public Shooting shooting;
    public bool testMode;  // if testMode is marked the wave will be re-generated after 3 sec

    private ICurvePath curvePath = new CurvePathSpline();

    private void Start()
    {
        StartCoroutine(CreateEnemyWave());
    }

    IEnumerator CreateEnemyWave()
    {
        for(int i = 0; i < count; i++)
        {
            GameObject newEnemy = Instantiate(enemy, enemy.transform.position, Quaternion.identity);
            FollowThePath followThePath = newEnemy.GetComponent<FollowThePath>();
            followThePath.SetPathType(curvePath);
            followThePath.SetPath(pathPoints, speed, rotationByPath, Loop);
            // configuring enemy shot parameters
            newEnemy.SetActive(true);
            yield return new WaitForSeconds(timeBetween);
        }
        if (testMode)
        {
            yield return new WaitForSeconds(3);
            StartCoroutine(CreateEnemyWave());
        }
        else if (!Loop)
            Destroy(gameObject);
    }

    void OnDrawGizmos()
    {
        curvePath.DrawPath(pathPoints, pathColor);
    }
}

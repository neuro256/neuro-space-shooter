﻿using System;
using UnityEngine;

public class CurvePathSpline : ICurvePath
{
    public Vector3 CalcCurve(Vector3[] path, float t)
    {
        return Interpolate(CreatePoints(path), t);
    }

    private Vector3 Interpolate(Vector3[] path, float t)
    {
        int numSections = path.Length - 3;
        int currPt = Mathf.Min(Mathf.FloorToInt(t * numSections), numSections - 1);
        float u = t * numSections - currPt;
        Vector3 a = path[currPt];
        Vector3 b = path[currPt + 1];
        Vector3 c = path[currPt + 2];
        Vector3 d = path[currPt + 3];
        return 0.5f * ((-a + 3f * b - 3f * c + d) * (u * u * u) + (2f * a - 5f * b + 4f * c - d) * (u * u) + (-a + c) * u + 2f * b);
    }

    private Vector3[] CreatePoints(Vector3[] path)  //using interpolation method calculating the path along the path points
    {
        Vector3[] pathPositions;
        Vector3[] newPathPos;
        int dist = 2;
        pathPositions = path;
        newPathPos = new Vector3[pathPositions.Length + dist];
        Array.Copy(pathPositions, 0, newPathPos, 1, pathPositions.Length);
        newPathPos[0] = newPathPos[1] + (newPathPos[1] - newPathPos[2]);
        newPathPos[newPathPos.Length - 1] = newPathPos[newPathPos.Length - 2] + (newPathPos[newPathPos.Length - 2] - newPathPos[newPathPos.Length - 3]);
        if (newPathPos[1] == newPathPos[newPathPos.Length - 2])
        {
            Vector3[] LoopSpline = new Vector3[newPathPos.Length];
            Array.Copy(newPathPos, LoopSpline, newPathPos.Length);
            LoopSpline[0] = LoopSpline[LoopSpline.Length - 3];
            LoopSpline[LoopSpline.Length - 1] = LoopSpline[2];
            newPathPos = new Vector3[LoopSpline.Length];
            Array.Copy(LoopSpline, newPathPos, LoopSpline.Length);
        }
        return (newPathPos);
    }

    public void DrawPath(Transform[] path, Color pathColor)
    {
        Vector3[] pathPositions = new Vector3[path.Length];
        for (int i = 0; i < path.Length; i++)
        {
            pathPositions[i] = path[i].position;
        }
        Vector3[] newPathPositions = CreatePoints(pathPositions);
        Vector3 previosPositions = Interpolate(newPathPositions, 0);
        Gizmos.color = pathColor;
        int SmoothAmount = path.Length * 20;
        for (int i = 1; i <= SmoothAmount; i++)
        {
            float t = (float)i / SmoothAmount;
            Vector3 currentPositions = Interpolate(newPathPositions, t);
            Gizmos.DrawLine(currentPositions, previosPositions);
            previosPositions = currentPositions;
        }
    }
}

public class CurvePathBezier : ICurvePath
{
    public Vector3 CalcCurve(Vector3[] path, float t)
    {
        Vector3 a = path[0];
        Vector3 b = path[1];
        Vector3 c = path[2];
        Vector3 d = path[3];

        return (1 - t) * (1 - t) * (1 - t) * a + 3 * (1 - t) * (1 - t) * t * b + 3 * (1 - t) * (t * t) * c + (t * t * t) * d;
    }

    public void DrawPath(Transform[] path, Color pathColor)
    {
        Vector3[] pathPositions = new Vector3[path.Length];
        for (int i = 0; i < path.Length; i++)
        {
            pathPositions[i] = path[i].position;
        }
        Gizmos.color = pathColor;
        int SmoothAmount = path.Length * 20;
        Vector3 previosPosition = CalcCurve(pathPositions, 0);
        for (int i = 0; i <= SmoothAmount; i++)
        {
            float t = (float)i / SmoothAmount;
            Vector3 currentPositions = CalcCurve(pathPositions, t);
            Gizmos.DrawLine(currentPositions, previosPosition);
            previosPosition = currentPositions;
        }
    }

    private Vector3[] TransformCurveFromSquareToCubic(Vector3[] source)
    {
        Vector3[] result = new Vector3[4];
        Vector3 a = source[0];
        Vector3 b = source[1];
        Vector3 c = source[3];

        result[0] = a;
        result[1] = a + (2f * (b - a)) / 3f;
        result[2] = b + (c - b) / 3f;
        result[3] = c;

        return result;
    }
}

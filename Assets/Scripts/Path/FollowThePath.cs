﻿using System;
using UnityEngine;

public class FollowThePath : MonoBehaviour {

    private float speed;
    private bool rotationByPath;
    private bool loop;
    private float currentPathPercent;
    private Vector3[] pathPositions;
    [HideInInspector] public bool movingIsActivate;

    private ICurvePath curvePath;

    public void  SetPathType(ICurvePath pCurvePath)
    {
        curvePath = pCurvePath;
    }

    public void SetPath(Transform[] p_PathPoints, float p_speed, bool p_RotationByPath, bool p_Loop)
    {
        speed = p_speed;
        rotationByPath = p_RotationByPath;
        loop = p_Loop;
        currentPathPercent = 0;
        pathPositions = new Vector3[p_PathPoints.Length];
        for (int i = 0; i < pathPositions.Length; i++)
        {
            pathPositions[i] = p_PathPoints[i].position;
        }
        transform.position = NewPositionByPath(pathPositions, 0);
        if (!rotationByPath)
            transform.rotation = Quaternion.identity;
        movingIsActivate = true;
    }

    Vector3 NewPositionByPath(Vector3[] pathPos, float percent)
    {
        return curvePath.CalcCurve(pathPos, percent);
    }

    private void Update()
    {
        if(movingIsActivate)
        {
            currentPathPercent += speed / 100 * Time.deltaTime;

            transform.position = NewPositionByPath(pathPositions, currentPathPercent);
            if(rotationByPath)
            {
                transform.right = NewPositionByPath(pathPositions, currentPathPercent + 0.01f) - transform.position; 
                transform.Rotate(Vector3.forward * 90);
            }
            if(currentPathPercent > 1)
            {
                if (loop)
                    currentPathPercent = 0;
                else
                {
                    Destroy(gameObject);
                }
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotator : MonoBehaviour {

    public float tumble; // модификатор скорости вращения 

    private void Start()
    {
        GetComponent<Rigidbody2D>().angularVelocity = Random.Range(0.0f, 1.0f) * tumble;
    }
}

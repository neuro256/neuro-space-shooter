﻿using System.Collections.Generic;
using UnityEngine;

public class HealthViewer : MonoBehaviour {

    public GameObject healthObj;
    public float healthPointsOffset = 0.5f;

    private List<GameObject> healthPoints;

    public void CreateHealthCounter(int healthPointsCount)
    {
        healthPoints = new List<GameObject>();
        float spriteSizeX = healthObj.GetComponent<RectTransform>().rect.width;
        for (int i = 0; i < healthPointsCount; i++)
        {
            Debug.Log(transform.position + Vector3.right * i * (spriteSizeX + healthPointsOffset));
            healthPoints.Add(Instantiate(healthObj, transform.position + Vector3.right * i * (spriteSizeX + healthPointsOffset), transform.rotation, transform));
        }
    }

    public void UpdateHealthCounter(int currentPoints)
    {
        for (int i = 0; i < healthPoints.Count; i++)
        {
            if (i >= currentPoints)
            {
                healthPoints[i].SetActive(false);
            }
            else
            {
                healthPoints[i].SetActive(true);
            }
        }
    }
}

﻿using System.Collections;
using UnityEngine;

public class WeaponController : MonoBehaviour {

    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;
    public float delay;
    public bool IsBomb = false;

    private AudioSource audioSource;

	// Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(Fire());
	}

    private IEnumerator Fire()
    {
        yield return new WaitForSeconds(delay + Random.Range(-delay / 2.0f, delay / 2.0f));

        while (true)
        {
            if (IsBomb)
            {
                Instantiate(shot, shotSpawn.position, Quaternion.LookRotation(Vector3.zero));
            }
            else
            {
                Instantiate(shot, shotSpawn.position, transform.rotation);
            }
            audioSource.Play();

            yield return new WaitForSeconds(fireRate + Random.Range(-fireRate / 2.0f, fireRate / 2.0f));
        }
    }
}

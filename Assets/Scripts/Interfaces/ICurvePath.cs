﻿using UnityEngine;

public interface ICurvePath
{
    Vector3 CalcCurve(Vector3[] path, float t);
    void DrawPath(Transform[] path, Color pathColor);
}
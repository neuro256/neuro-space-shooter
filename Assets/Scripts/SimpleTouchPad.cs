﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SimpleTouchPad : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler, IPointerExitHandler {

    public float smoothing;

    private Vector2 origin;
    private Vector2 direction;
    private Vector2 smoothDireciton;
    private bool touched;
    private int pointerID;

    private void Awake()
    {
        direction = Vector2.zero;
        touched = false;
    }

    public void OnPointerDown(PointerEventData data)
    {
        if (!touched)
        {
            touched = true;
            pointerID = data.pointerId;
            origin = data.position;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        // disable drag when on pointer exit gameobject
        //if(eventData.pointerId == pointerID && touched)

        if (eventData.pointerId == pointerID)
        {
            Vector2 currentPosition = eventData.position;
            Vector2 directionRaw = currentPosition - origin;
            direction = directionRaw.normalized;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if(eventData.pointerId == pointerID)
        {
            EndMove();
        }
    }

    public Vector2 GetDirection()
    {
        smoothDireciton = Vector2.MoveTowards(smoothDireciton, direction, smoothing);
        return smoothDireciton;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(eventData.pointerId == pointerID)
        {
            EndMove();
            Debug.Log("End Move");
        }
    }

    private void EndMove()
    {
        direction = Vector3.zero;
        touched = false;
    }
}
